# DnA (Data n' Archive)

DnA is a 2.4PB GPFS file system for the 
[Joint Genome Institute](https://jgi.doe.gov/) (JGI)'s archive, shared
databases, and project directories. Write access to DnA is restricted
to protect high performance; data can only be written to DnA from
[Data Transfer Nodes](../systems/dtn/index.md).

| 	|DnA Projects|DnA Shared|DnA DM Archive|
|---|---|---|---|
|Location|`/global/dna/projectdirs/`|`/global/dna/shared`|`/global/dna/dm_archive`|
|Quota|5TB default|Defined by agreement with the JGI Management|Defined by agreement with the JGI Management|
|Backups|Daily, only for projectdirs with quota <= 5TB|Backed up by JAMO|Backed up by JAMO|
|File purging|Files are not automatically purged|Purge policy set by users of the JAMO system|Files are not automatically purged|

The intention of the DnA "Project" and "Shared" space is to be a place
for data that is needed by multiple users collaborating on a project
which allows for easy reading of shared data. The "Project" space is
owned and managed by the JGI.  The "Shared" space is a collaborative
effort between the JGI and NERSC.

If you would like a project directory, please contact JGI management to
discuss the use case and requirements for the proposed directory.

The "DM Archive" is a data repository maintained by the JAMO system.
Files are stored here during migration using the JAMO system.  The
files can remain in this space for as long as the user specifies.  Any
file that is in the "DM Archive" has also been placed in the HPSS tape
archive.  This section of the file system is owned by the JGI data
management team.

!!! note
	JAMO is JGI's in-house-built hierarchical file system, which
	has functional ties to NERSC's file systems and tape
	archive. JAMO is not maintained by NERSC.
