#!/bin/bash
#SBATCH -N 2
#SBATCH -C cpu
#SBATCH -q regular
#SBATCH -t 01:00:00
#SBATCH -J vasp_job
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err

# Default version loaded: vasp/5.4.4-cpu
module load vasp

# Run with (-n) 256 total MPI ranks
#  128-MPI-ranks-per-node is maximum on Perlmutter CPU
# Set -c ("--cpus-per-task") = 2 
#  to space processes two "logical cores" apart
srun -n 256 -c 2 --cpu-bind=cores vasp_std
